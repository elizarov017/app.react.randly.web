import './App.css';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from "react-router-dom";
import HeaderComponent from './Components/Header/HeaderComponent';
import HomePageComponent from './Components/Pages/HomePage/HomePageComponent';
import InspirationPageComponent from './Components/Pages/InspirationPage/InspirationPageComponent';
import SearchPageComponent from './Components/Pages/SearchPage/SearchPageComponent';
import AboutPageComponent from './Components/Pages/AboutPageComponent/AboutPageComponent';

function App() {
  return (


    <div className="App">
      <HeaderComponent />
      <Router>
        <Routes>
            <Route exact path="/" element={<Navigate to="/home"/>}/>
            <Route path="/home" element={<HomePageComponent/>}/>
            <Route path="/inspiration" element={<InspirationPageComponent/>}/>
            <Route path="/search" element={<SearchPageComponent/>}/>
            <Route path="/about" element={<AboutPageComponent/>}/>
          </Routes>
      </Router>
      
      
    </div>
  );
}

export default App;
