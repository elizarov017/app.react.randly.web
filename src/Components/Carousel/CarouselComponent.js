import React from 'react'
import Carousel from 'react-bootstrap/Carousel';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'holderjs'
import "./CarouselComponent.css"
import CarouselCaption from './ItemComponent/CarouselCaption';



export default function CarouselComponent({photos}) {

    return (
        <Carousel>
            <Carousel.Item>
                <img
                className="d-block"
                src={photos[0].urls.full}
                alt="First slide"
                />
                <Carousel.Caption>
                    <CarouselCaption photo={photos[0]} heading={"One of the biggest photo storage in the world"}/>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block"
                src={photos[1].urls.full}
                alt="Second slide"
                />

                <Carousel.Caption>
                    <CarouselCaption photo={photos[1]} heading={"More than 3.4M photos"}/>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                className="d-block"
                src={photos[2].urls.full}
                alt="Third slide"
                />

                <Carousel.Caption>
                    <CarouselCaption photo={photos[2]} heading={"And 275.3k photographers"}/>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    
    )
}
