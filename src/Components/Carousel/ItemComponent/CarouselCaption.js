import React from 'react'
import  './CarouselCaption.css'

export default function CarouselCaption({photo, heading}) {
    return (
        <>
            <span style={{fontSize: "32px"}}>{heading}</span><br/>
            <span>Uploaded by {photo.user.name}.</span>
        </>
    )
}
