import React, { useEffect, useState } from 'react'
import { Container } from 'react-bootstrap'
import Loader from 'react-loader-spinner'
import { getListOfRandomImages } from '../../../Utils/utils'
import Heading from '../../Heading/Heading'
import PicFooter from '../../PicFooter/PicFooter'

export default function AboutPageComponent() {

    const importPhotos = 1

    const [photos, setPhotos] = useState([])
    const [isLoaded, setIsLoaded] = useState(false)

    useEffect(() => {
        setPhotos(getListOfRandomImages(importPhotos))
    }, [])

    useEffect(() => {
        if (photos[importPhotos - 1])
            setIsLoaded(true)
    }, [photos])

    return (
        <>
            <Container>
                <br/>
                <br/>
                <Heading text={"About Us"}/>
                <p>Email: yaroshepta.b@gmail.com</p>
                <p>Country: Ukraine</p>
                <p>Address:</p>
                <iframe 
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1332.1127101560385!2d30.518805096458653!3d50.46476081782662!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa2a7cb05906c30aa!2zNTDCsDI3JzUxLjciTiAzMMKwMzEnMDkuMCJF!5e0!3m2!1sru!2sua!4v1639111852764!5m2!1sru!2sua" 
                    width="600" 
                    height="450" 
                    style={{"border": 0, "max-width": "90%"}} 
                    allowfullscreen="" 
                    loading="lazy"></iframe>
            </Container>
            <hr/>
            <br/>
            {
                !isLoaded ?
                <Loader
                    type="Puff"
                    color="#00BFFF"
                    height={100}
                    width={100}
                />
                :
                <PicFooter photo={photos[0]}/>
            }
        </>
    )
}
