import React, {useEffect, useState} from 'react'
import BgSlideComponent from '../../BgSlide/BgSlideComponent'
import CarouselComponent from '../../Carousel/CarouselComponent'
import {getListOfRandomImages, getBase64FromUrl} from '../../../Utils/utils.js'
import HomeInfoComponent from '../../HomeInfo/HomeInfoComponent'
import Heading from '../../Heading/Heading'
import Loader from "react-loader-spinner";
import PicFooter from '../../PicFooter/PicFooter'

export default function HomePageComponent() {

    const importPhotos = 6

    const [photos, setPhotos] = useState([])
    const [isLoaded, setIsLoaded] = useState(false)

    useEffect(() => {
        setPhotos(getListOfRandomImages(importPhotos, "landscape"))
    }, [])

    useEffect(() => {
        if (photos[importPhotos - 1])
            setIsLoaded(true)
    }, [photos])

    return (
        !isLoaded ?
        <Loader
            type="Puff"
            color="#00BFFF"
            height={100}
            width={100}
            style={{marginTop: "200px"}}
        />
        :
        <>
            <CarouselComponent photos={photos}/>
            <Heading text={"Randly - your helper with inspiration!"}/>
            <BgSlideComponent/>
        
            <HomeInfoComponent photos={photos}/>

            <hr/>
            <PicFooter photo={photos[5]}/>
        </>
    )
}
