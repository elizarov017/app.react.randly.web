import React, { useEffect, useState } from 'react'
import { Container, FloatingLabel, Form, Button, Pagination } from 'react-bootstrap'
import "./SearchPageComponent.css"
import Loader from 'react-loader-spinner'
import { getListOfImagesInOrder, getListOfRandomImages } from '../../../Utils/utils'
import BgSlideComponent from '../../BgSlide/BgSlideComponent'
import Heading from '../../Heading/Heading'
import PicFooter from '../../PicFooter/PicFooter'
import PhotoListComponent from '../../PhotoListComponent/PhotoListComponent'

export default function SearchPageComponent() {
    const importPhotos = 1

    const [photos, setPhotos] = useState([])
    const [isLoadedFooter, setIsLoadedFooter] = useState(false)
    const [footerPhoto, setFooterPhoto] = useState([])
    const [isLoadedPhotos, setIsLoadedPhotos] = useState(false)
    const [page, setPage] = useState(1)
    const [orderBy, setOrderBy] = useState('latest')

    function handleChange(event) {
        setOrderBy(event.target.value)
    }

    function handleOrder() {
        setPhotos(getListOfImagesInOrder(1, orderBy))
        setPage(1)
    }

    function handleNextPage() {
        if (page < 10) {
            setPhotos(getListOfImagesInOrder(page + 1, orderBy))
            setPage(page + 1)
            setIsLoadedPhotos(false)
        }
    }
    
    function handlePrevPage() {
        if (page > 1) {
            setPhotos(getListOfImagesInOrder(page - 1, orderBy))
            setPage(page - 1)
            setIsLoadedPhotos(false)
        }
    }
    useEffect(() => {
        setFooterPhoto(getListOfRandomImages(importPhotos))
        setPhotos(getListOfImagesInOrder(page, orderBy))
    }, [])

    useEffect(() => {
        if (photos[0]) {
            setIsLoadedPhotos(true)
        }

    }, [photos])
    
    useEffect(() => {
        if (footerPhoto[0]) {
            setIsLoadedFooter(true)
        }

    }, [footerPhoto])
    return (
        <>
            <Container className={"search-container"}> 
                <Heading text={"Top Photos"}/>
                <div className={"form-group"}>
                    <FloatingLabel className={"order-by-form"} controlId="floatingSelectGrid" label="Order By">
                        <Form.Select value={orderBy} onChange={handleChange} aria-label="Order by">
                            <option value="latest">Latest</option>
                            <option value="oldest">Oldest</option>
                            <option value="popular">Popular</option>
                        </Form.Select>
                    </FloatingLabel>
                    <Button variant="outline-secondary" onClick={handleOrder} type="submit">
                        Load
                    </Button>
                </div>
            </Container>
            <BgSlideComponent/>
            <hr/>
            {!isLoadedPhotos ?
                <Loader
                    type="Puff"
                    color="#00BFFF"
                    height={100}
                    width={100}
                />
                :
                <Container className={"photos-container"}>
                    <PhotoListComponent photos={photos}/>
                </Container>
                }

                <Container className={"d-flex justify-center"}>
                    <Pagination>
                        <Pagination.Prev variant="outline-secondary" onClick={handlePrevPage}/>
                        <Pagination.Item variant="secondary" disabled={true} active>{page}</Pagination.Item>
                        <Pagination.Next variant="outline-secondary" onClick={handleNextPage}/>
                    </Pagination>
                </Container>

            {!isLoadedFooter ?
                ""
            :
            <>
                <hr/>
                <PicFooter photo={footerPhoto[0]}/>
            </>
            }
        </>
    )
}
