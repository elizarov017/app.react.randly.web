import React, {useEffect, useState} from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import {getListOfRandomImages, convertPhotosToGallery} from '../../../Utils/utils.js'
import Gallery from 'react-photo-gallery'
import Loader from 'react-loader-spinner'
import "./InspirationPageComponent.css"

export default function InspirationPageComponent() {

    const perRequest = 30

    const [photos, setPhotos] = useState([])
    const [isLoaded, setIsLoaded] = useState(false)
    const [page, setPage] = useState(1)

    useEffect(() => {
        getPhotos()
    }, [])

    useEffect(() => {
        if (photos[1])
            setIsLoaded(true)
    }, [photos])

    function getPhotos() {
        let fetch = getListOfRandomImages(perRequest)
        setPage(page + 1)
        
        setPhotos(photos.concat(fetch))
        setTimeout(() => {
            setIsLoaded(true)
        }, 1000);
    }



    return (
        <>
            {isLoaded ? 
                <InfiniteScroll
                    dataLength={photos.length}
                    next={getPhotos}
                    hasMore={true}
                    loader={<Loader
                        type="Puff"
                        color="#00BFFF"
                        height={100}
                        width={100}
                    />}
                    endMessage={
                        <p style={{ textAlign: 'center' }}>
                        <b>Yay! You have seen it all</b>
                        </p>
                    }>
                    <Gallery photos={convertPhotosToGallery(photos)}/>
                </InfiniteScroll>
            
            :
                ""
            }
            
        </>
    )
}
