import React from 'react'
import { Col, Image, Row } from 'react-bootstrap'
import "./InfoSectionComponent.css"

export default function InfoSectionComponent({photo, left=false}) {
    return (
        <>
        {
            left ?
                <Row className={"info-section"}>
                    <Col className={"text-paragraph"} md={7} sm={12}>
                        <span>
                            The origins of the relationship between imagery and text can be traced back well before the existence of photography. 
                            Sketches, paintings and illustrations were used to illuminate endless, streaming blocks of letters, telling us stories, 
                            recording history and disseminating news for thousands of years, so that by the time photography was invented, the role of 
                            the image in textual storytelling was a given.
                        </span> 
                    </Col>
                    <Col  md={5} sm={12}>
                        <div className={"pic-wrapper"}>
                            <Image className={"info-pic"} src={photo.urls.regular} rounded/>
                        </div>
                    </Col>
                </Row>
            :
                <Row className={"info-section"}>
                    <Col  md={5} sm={12}>
                        <div className={"pic-wrapper"}>
                            <Image className={"info-pic"} src={photo.urls.regular} rounded/>
                        </div>
                    </Col>
                    <Col className={"text-paragraph"} md={7} sm={12}>
                        <span>
                            Aside from this collaboration, the work speaks about a greater existential conundrum: accepting one’s past. 
                            Salvati’s words dig into experiences desired to be forgotten, and Mariani’s photographs act as relics of memories and fleeting moments. 
                            They invite us to see the evolution that comes from our past actions, re-contextualizing our scars as 
                            something more akin to beauty marks.
                        </span> 
                    </Col>
                </Row>
        }
        </>
    )
}
