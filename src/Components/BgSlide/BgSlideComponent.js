import React from 'react'
import './BgSlideComponent.css'

export default function BgSlideComponent() {
    return (
        <div className="bg-slide-container">
            <div className="bg-slide"></div>
        </div>
    )
}
