import React from 'react'
import { Container } from 'react-bootstrap'
import Heading from '../Heading/Heading'
import InfoSectionComponent from '../InfoSection/InfoSectionComponent'

export default function HomeInfoComponent({photos}) {
    return (
        <Container>
            <Heading text={"Burn-out? Bad mood? We have an answer!"}/>
            <InfoSectionComponent photo={photos[3]} text={"kek"}/>
            <Heading text={"Or just want to look for some photos?"}/>
            <InfoSectionComponent photo={photos[4]} left text={"kek"}/>
        </Container>
    )
}
