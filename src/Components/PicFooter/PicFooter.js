import React from 'react'
import './PicFooter.css'

export default function PicFooter({photo}) {

    const footerStyles ={
        backgroundImage: 'url(' + photo.urls.full + ')',
        backgroundSize: 'cover',
        backgroundPosition: 'center'
    }

    return (
        <footer className={"pic-footer"} style={footerStyles}>
            <div className={"footer-bg"}/>
            <span className={""}>RANDLY</span>
        </footer>
    )
}
