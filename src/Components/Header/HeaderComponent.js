import React, {useEffect} from 'react'
import { Navbar, Container, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap';

import './HeaderComponent.css'

export default function HeaderComponent() {



    return (
        <>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="/home">Randly</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <div className="vr"></div>
                        <Nav.Link href="/inspiration">Look for inspiration!</Nav.Link>
                        <div className="vr"></div>
                        <NavDropdown title="More" id="basic-nav-dropdown">
                        <NavDropdown.Item href="/search">Search Photos</NavDropdown.Item>
                        <NavDropdown.Item target="_blank" href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">Secret Link</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="/about">About Us</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                        <Navbar.Text>
                            Developed by <a href="https://www.linkedin.com/in/bohdan-yaroshepta-581b6520a/" target="_blank">Yaroshepta B.</a>
                        </Navbar.Text>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}
