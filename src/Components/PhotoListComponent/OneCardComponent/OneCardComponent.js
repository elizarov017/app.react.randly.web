import React from 'react'
import { Card } from 'react-bootstrap'
import './OneCardComponent.css'

export default function OneCardComponent({photo}) {
    return (
        <Card style={{ width: '18rem' }}>
            <div className={"img-wrapper"}>
                <Card.Img variant="top" src={photo.urls.small}/>
            </div>
            <Card.Body>
                <Card.Title className={"m-0"}>By {photo.user.name}</Card.Title>
            </Card.Body>
        </Card>
    )
}
