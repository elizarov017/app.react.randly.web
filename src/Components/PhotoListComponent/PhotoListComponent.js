import React from 'react'
import OneCardComponent from './OneCardComponent/OneCardComponent'

export default function PhotoListComponent({photos}) {
    return (
        <>
            {
                photos.map(photo => {
                    return <OneCardComponent key={photo.id} photo={photo}/>
                })
            }
        </>
    )
}
