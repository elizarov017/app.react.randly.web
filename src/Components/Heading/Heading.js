import React from 'react'

export default function Heading({text}) {
    return (
        <>
            <h1 style={{margin: "20px 0"}}>{text}</h1>
        </>
    )
}
