
const baseUrl = 'https://api.unsplash.com/'

function initCounter() {
    const {tokens} = require("../auth.js")
    let count = 0
    const maxCount = tokens.length
    return () => {
        if (count == maxCount) count = 0
        return tokens[count++]
    }
}

var getToken = initCounter();



export function getListOfRandomImages(count, orientation) {
    let url = new URL(baseUrl)
    let params = new URLSearchParams(url.search)

    if (count) params.set('count', count)
    if (orientation) params.set('orientation', orientation)

    return httpGet(url + 'photos/random/?' + params.toString())
}

export function getListOfImagesInOrder(page, orderBy) {
    let url = new URL(baseUrl)
    let params = new URLSearchParams(url.search)

    if (page) params.set('page', page)
    if (orderBy) params.set('order_by', orderBy)

    return httpGet(url + 'photos/?' + params.toString())
}

export function capitalizeFirstLetter(string) {
    return string ? string.charAt(0).toUpperCase() + string.slice(1) : "";
}

export function convertPhotosToGallery(photos) {
    return photos.map(photo => {
        return {
            src: photo.urls.regular,
            caption: `Uploaded by ${photo.user.name}. ${capitalizeFirstLetter(photo.description)}`,
            width: parseFloat(photo.width) / 10,
            height: parseFloat(photo.height) / 10,
            className: "gallery-item"
        }
    })
}

function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.setRequestHeader('Authorization', `Client-ID ${getToken()}`)
    xmlHttp.send( null );
    return JSON.parse(xmlHttp.responseText);
}



export function getBase64FromUrl(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
      var reader = new FileReader();
      reader.onloadend = function() {
          callback(reader.result);
      }
      reader.readAsDataURL(xhr.response);
  };
  xhr.open('GET', url);
  xhr.responseType = 'blob';
  xhr.send();
}

